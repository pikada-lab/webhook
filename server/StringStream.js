const { Readable } = require('stream');

class StringStream extends Readable {
    constructor(str, encoding) {
        super()
        this._str = str
        this._encoding = encoding || 'utf8'
    }

    _read() {
        if (!this.ended) {
            this.push(Buffer.from(this._str, this._encoding))
            this.push(null)
            this.ended = true
        }
    }
}

module.exports.StringStream = StringStream;