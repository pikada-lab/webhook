const http = require('http');
const { ResponseApi, ResponseError } = require('./ResponseApi');
const { StringStream } = require('./StringStream');
const { pipeline } = require('stream');
const { readFile } = require('fs');
const zlib = require('zlib');
const { promisify } = require('util');
const { join } = require('path');
const { AuthorizationFuctory } = require('./AuthorizationFactory');

class Server {

    constructor(config, versionController) {
        this.config = config;
        this.versionController = versionController;
        this.auth = AuthorizationFuctory(config.auth);
    }
    start() {
        const server = http.createServer((req, res) => {

            console.log(`[${req.method}] ${req.url}`);
            if (this.faviconRequest(res, req)) return;
            if (req.url.startsWith("/v1/")) {
                this.startVersion1(res, req);
                return;
            } else {
                // Ответ обрабатывается не стандартным парсером версии, поэтому errorResponse не используется
                res.writeHead(404, { 'Content-Type': 'application/json' });
                res.end(ResponseError(100, "not support request"));
                return;
            }
        });

        server.on('clientError', (err, socket) => {
            socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
        });
        server.listen(this.config.server.port);
    }

    startVersion1(res, req) {
        this.readBody(req).then(body => {

            res.setHeader("Access-Control-Allow-Origin", "*");
            res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");

            if (!this.auth) {
                this.errorResponse(res, { status: 104, message: "not authorization system" });
                return;
            }

            if (!this.auth.check(req)) {
                this.errorResponse(res, { status: 105, message: "not authentic header" });
                return;
            }

            let url = new URL(req.url, `http://${req.url.host}`)

            let innerPath = url.pathname.substr(4);
            let method = req.method.toLowerCase();
            let params = url.searchParams;

            return this.versionController.getController(1).handle(innerPath, method, params, body);
        }).then(text => {
            this.sendResponseFromText(res, req, text);
        }).catch(err => {
            console.log(err)
            this.errorResponse(res, err);
        });
    }

    sendResponseFromText(res, req, text) {
        if (typeof text != 'undefined') {
            this.sendResponse(res, req, ResponseApi(text));
        } else {
            this.errorResponse(res, { status: 101, message: "not support request" });
        }
    }
    readBody(req) {
        return new Promise((resolve, reject) => {

            var body = ''
            req.on('data', function(data) {
                body += data
            })
            req.on('error', function(err) {
                reject(err)
            })

            req.on('end', function() {
                if (body) {
                    try {
                        body = JSON.parse(body);
                    } catch (ex) {
                        reject({ status: 11, message: ex.message });
                        return;
                    }
                }
                resolve(body)
            })
        })
    }
    faviconRequest(res, req) {
        if (req.url !== '/favicon.ico') return false;
        res.setHeader('Content-Type', 'image/x-icon');
        res.writeHead(200, { 'Content-Type': 'image/x-icon' });
        promisify(readFile)(join(this.config.root, "favicon.ico")).then(img => {
            res.end(img);
        });
        return true;
    }
    errorResponse(res, err) {
        let status = 10;
        if (typeof err == 'object') {
            if (typeof err.errno != 'undefined')
                status = err.errno;
            if (typeof err.sqlMessage != 'undefined')
                err = err.sqlMessage
            if (typeof err.status != 'undefined')
                status = err.status;
            if (typeof err.message != 'undefined')
                err = err.message;
        }
        res.writeHead(401, { 'Content-Type': 'application/json' });
        res.end(ResponseError(status, err));
    }
    sendResponse(res, req, responseApiString) {

        res.setHeader('Content-Type', 'application/json');
        res.setHeader('Vary', 'Accept-Encoding');
        let acceptEncoding = req.headers['accept-encoding'];
        if (!acceptEncoding) {
            acceptEncoding = '';
        }
        const onError = (err) => {
            if (err) {
                console.error('An error occurred:', err);
                res.end();
            }
        };

        const raw = new StringStream(responseApiString, 'utf8');

        if (/\bdeflate\b/.test(acceptEncoding)) {
            console.log("deflate encoding");
            res.writeHead(200, { 'Content-Encoding': 'deflate' });
            pipeline(raw, zlib.createDeflate(), res, onError);
        } else if (/\bgzip\b/.test(acceptEncoding)) {
            console.log("gzip encoding");
            res.writeHead(200, { 'Content-Encoding': 'gzip' });
            pipeline(raw, zlib.createGzip(), res, onError);
        } else if (/\bbr\b/.test(acceptEncoding)) {
            console.log("br encoding");
            res.writeHead(200, { 'Content-Encoding': 'br' });
            pipeline(raw, zlib.createBrotliCompress(), res, onError);
        } else {
            console.log("not encoding");
            res.writeHead(200, {});
            pipeline(raw, res, onError);
        }
    }
}

module.exports.Server = Server;