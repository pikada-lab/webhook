var ResponseApi = (response, status, message) => {
    return JSON.stringify({
        response: response,
        status: status ? status : 0,
        message: message ? message : ""
    })
}

module.exports.ResponseError = (status, message) => {
    return ResponseApi(null, (status ? status : 1), message);
}

module.exports.ResponseApi = ResponseApi;