class BasicAuthorization {
    constructor(config) {
        this.auth = `${config.type} ` + Buffer.from(`${config.login}:${config.password}`).toString('base64');
    }
    check(req) {
        let sign = req.headers['authorization'];
        if (!sign) return false;
        if (sign === this.auth) return true;
        return false;
    }
}

module.exports.BasicAuthorization = BasicAuthorization;