const { BasicAuthorization } = require("./BasicAuthorization");

module.exports.AuthorizationFuctory = (config) => {
    switch (config.type) {
        case 'Basic':
            return new BasicAuthorization(config);
        default:
            return null;
    }
}