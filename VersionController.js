const { V1Controller } = require("./v1/V1Controller");

class VersionController {
    constructor(connect) {
        this.connect = connect;
        this.version1 = new V1Controller(connect);
    }

    getController(version) {
        if (version == 1) {
            return this.version1;
        }
        return null;
    }
}
module.exports.VersionController = VersionController