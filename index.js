const { App } = require("./app");

const app = new App(process.env);
app.start().then();