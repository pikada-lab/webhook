const { ClientSocket } = require('./ClientSocket');
const { appendFileSync } = require('fs');
const { EOL } = require('os');
const client = new ClientSocket({
    port: 8124,
    host: 'localhost',
    name: "auxilium-server",
    key: "./private-key.pem",
    cert: "./public-cert.pem"
});
client.init();

let i = 0;
setInterval(_ => {
    client.send('helth', JSON.stringify({ version: i++ }));
}, 2000)
client.events.on("connect", _ => {
    console.log("connected");
})
client.events.on('server/events', (data) => {
    console.log("events", data);
});
client.events.addListener("server/message/update", data => {
    // console.log("hi", data);
    appendFileSync("./data.json", data + EOL, (err) => {
        console.log(err);
    });
})
client.events.addListener("server/message/delete", data => {
    // console.log("hi", data);
    appendFileSync("./data.json", data + EOL, (err) => {
        console.log(err);
    });
})
client.events.addListener("server/message/add", data => {
    // console.log("hi", data);
    appendFileSync("./data.json", data + EOL, (err) => {
        console.log(err);
    });
})