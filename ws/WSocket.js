const EventEmitter = require("events");
const tls = require('tls');
const process = require('process');
const { readFileSync } = require('fs');

class WebEvents extends EventEmitter { }

class WSocket {
    #key;
    #cer;
    constructor(config) {
        this.events = new WebEvents();
        this.PORT = config?.port ?? 8124;
        this.HOST = config?.host ?? 'localhost';
        this.ping = config?.ping ?? false;
        this.connects = [];
        if (config.key) {
            this.enableEncript = true;
            this.#key = readFileSync(config.key)
            this.#cer = readFileSync(config.cert)
        } else {
            this.enableEncript = false;
        }
        this.LENGTH_BUFFER = 200;
        this.SOCKET_BUFFER = 65536;
        this.options = {
            key: this.#key,
            cert: this.#cer,
        };

        this.events.emit("construct", '');
    }

    init() {
        const server = tls.createServer(this.options, (connect) => {
            // 'connection' listener
            console.log('server connected',
                server.authorized ? 'authorized' : 'unauthorized');

            console.log('client connected');
            this.events.emit("connect", "");
            connect.on('end', () => {
                console.log('client disconnected');
                let id = -1;
                for (let i in this.connects) {
                    if (this.connects[i].stream == connect) {
                        id = this.connects[i].id;
                        delete this.connects[i];
                    }
                }
                this.events.emit("client/disconnected", id);
            });

            connect.once("data", (encryptResult) => {
                console.log("[connect]", encryptResult.toString());
                let [id, name] = encryptResult.toString().trim().split("\r\n");
                this.connects.push({
                    stream: connect,
                    id: id,
                    name: name
                });

                this.events.emit("client/connected", id);
                let result = "";
                connect.on("data", (data) => {
                    result += data.toString();
                    if (result.endsWith("\r\n")) {
                        this.#parseEvent(id, name, result);
                        result = "";
                    }
                })

            })
        });
        server.on('error', (err) => {
            this.events.emit("server/error", err);
        });
        server.listen(this.PORT, this.HOST, () => {
            console.log('Server listen', this.PORT, this.HOST);
            this.events.emit("server/listen", this.PORT);
        });

        if (this.ping) {
            this.events.addListener("server/listen", () => {
                setInterval(_ => {
                    this.sendToAll("ping", Math.ceil(process.hrtime()[0] * 1000000 + process.hrtime()[1] / 1000));
                }, 500)
            })
            this.events.addListener("client/message/*/pong", (data) => {

                let time = Math.ceil(process.hrtime()[0] * 1000000 + process.hrtime()[1] / 1000);

                const res = JSON.parse(data);
                console.log(`[${res.name}:${res.id}] send: ${res.pong - res.ping} µs, response: ${time - res.pong} µs, metric: ${time - res.ping} µs.`);
            })
        }
    }

    #parseEvent(id, name, fullResult) {
        this.#pushEvent(id, name, fullResult);
    }

    #pushEvent(id, name, chunk) {
        let [event, message] = chunk.toString().split(/:::/, 2);
        this.events.emit("client/message/" + id + "/" + event, message);
        this.events.emit("client/message/" + name + "/" + event, message);
        this.events.emit("client/message/*/" + event, message);
        this.events.emit("server/events", event);
    }

    getAll() {
        return this.connects.map(r => ({
            id: r.id,
            name: r.name
        }))
    }

    getIDs() {
        return this.connects.map(r => r.id);
    }

    getNames() {
        let set = this.connects.map(r => r.name).reduce((acc, cur) => {
            acc.add(cur)
            return acc;
        }, new Set());
        return Array.from(set);
    }

    hasNames(name) {
        let res = ~this.connects.indexOf(r => r.name == name);
        return res != 0;
    }

    sendToAll(eventName, message) {
        this.#sendToStreaam(this.connects.map(connect => connect.stream), eventName, message);
    }

    sendToName(eventName, name, message) {
        this.#sendToStreaam(
            this.connects
                .filter(connect => connect && connect.name == name)
                .map(connect => connect.stream)
            , eventName, message);
    }

    sendToID(eventName, id, message) {
        this.#sendToStreaam(this.connects
            .filter(connect => connect && connect.id == id)
            .map(connect => connect.stream), eventName, message);
    }

    #sendToStreaam(streams, eventName, message) {
        streams.forEach(ws => {
            console.time("send " + eventName)
            ws.write(eventName + ":::" + message + "\r\n");
            console.timeEnd("send " + eventName)
        });
    }

    getStream(id) {
        for (let connect of this.connects) {
            if (connect && connect.id == id) {
                return connect.stream;
            }
        }
        return null;
    }
}


module.exports.WSocket = WSocket;