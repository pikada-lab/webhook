
const tls = require('tls');
const EventEmitter = require("events");
const { v4: uuidv4 } = require('uuid');
const process = require('process');
const { readFileSync } = require('fs');

class WebEvents extends EventEmitter { }

class ClientSocket {
    #key
    #cert;
    constructor(config) {
        this.events = new WebEvents();
        this.ID = uuidv4();
        this.options = {}
        this.PORT = config?.port ?? 8124;
        this.options['port'] = this.PORT;
        this.HOST = config?.host ?? 'localhost';
        this.options['host'] = this.HOST;
        this.NAME = config?.name ?? "any client";
        this._client;
        this.stdin;
        this.stdout;
        this.connected = false;
        this.reconnect = true;
        this.SOCKET_BUFFER = 65536;

        this.#key = readFileSync(config.key);
        this.#cert = readFileSync(config.cert);

        Object.assign(this.options, {
            key: this.#key,
            cert: this.#cert, 
            rejectUnauthorized: false
        })
        this.events.emit("construct", this.ID);
    }

    #connect() {
        this._client = tls.connect(this.options, () => {
            console.log('connected to server!');
            this.events.emit("connect", this.ID);
            let data = Buffer.from(this.ID + '\r\n' + this.NAME + '\r\n');
            this._client.write(data);
            this.connected = true;
        });

        this._client.on('error', (e) => {
            if (e.code == 'ECONNREFUSED') {
                this.events.emit("server/error", e);
                this.connected = false;
                return false;
            }
        });

        this._client.on('end', () => {
            console.log("disconnect");
            this.connected = false;
            this.events.emit("disconnect", this.ID);
        });
        this.stdout = this._client;
        this.stream = this._client;
        this.stdin = this._client;
        let result = "";
        let i = 0;
        this.stdin.on("data", (data) => {
            console.log("> ", data.length, "[" + i + "]");
            result += data.toString();
            if (result.endsWith("\r\n")) {
                let fullResult = result;
                console.log(">>", fullResult.length, "[" + i + "]") // 539416 [9]
                result = "";
                i = 0;
                this.#pushEvent(fullResult)
            } else {
                i++
            }
        })
    }
    #pushEvent(encryptResult) {
        let [event, message] = encryptResult.split(/:::/, 2);
        this.events.emit("server/message/" + event, message);
        this.events.emit("server/events", event);
    }
    #reconnect() {
        this._reconnectID = setInterval(_ => {
            if (this.reconnect && !this.connected) {
                console.log("reconnect");
                this._client.destroy();
                this._client.unref();
                this.#connect();
            }
        }, 3000);
    }

    #reconnectOff() {
        console.log("reconnect off");
        this.reconnect = false;
        clearInterval(this._reconnectID);
    }

    init() {
        this.#connect();
        this.#reconnect();
        process.on("beforeExit", () => {
            this.#reconnectOff()
        })
        process.on("uncaughtException", (err) => {
            if (err.code == "ECONNREFUSED" && err.port == this.PORT) {
                console.log(err);
                console.log("uncaughtException", "ECONNREFUSED");
            } else {
                console.log(err);
                process.exit();
            }
        })
        this.onPingPong();
    }

    onPingPong() {
        this.events.addListener("server/message/ping", (ping) => this.#pong(ping));
    }

    #pong(ping) {
        const time = Math.ceil(process.hrtime()[0] * 1000000 + process.hrtime()[1] / 1000);
        this.send("pong", JSON.stringify({ ping: +ping, pong: time, id: this.ID, name: this.NAME }));
    }

    send(event, message) {
        let data = Buffer.from(event + ":::" + message + "\r\n");
        this.stdout.write(data + "\r\n");
    }

}

module.exports.ClientSocket = ClientSocket;