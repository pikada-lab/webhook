const { WSocket } = require("./WSocket.js");
const { readFile } = require("fs");
let s = new WSocket({
    key: "./private-key.pem",
    cert: "./public-cert.pem",
    port: 8124,
    host: 'localhost'
});

let interval = [];
s.init();
s.events.addListener("client/connected", (id) => {
    interval[id] = setInterval(() => {
        readFile("generated.json", (err, data) => {
            console.log(data.length);
            s.sendToID("hi", id, data.toString())
        });
    }, 10000)
})
s.events.addListener("client/disconnected", (id) => {
    console.log("disconnect event ", id);
    if (interval) {
        console.log("event clear", id);
        clearInterval(interval[id]);
    }
})
s.events.addListener("server/error", (err) => {
    console.error(err)
})
