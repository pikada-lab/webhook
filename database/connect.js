const mysql = require('mysql2');

class Connect {
    constructor(config) {
        this.connection = mysql.createPool({
            connectionLimit: config.connectionLimit,
            host: config.host,
            user: config.user,
            database: config.database,
            password: config.password
        });
    }

    async query(sql, params) {
        return new Promise((res, rej) => {
            this.connection.query(
                sql, params,
                function(err, results) {
                    if (err) {
                        return rej(err);
                    }
                    res(results);
                }
            );
        })
    }
}

module.exports.Connect = Connect;