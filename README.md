# Optic Studio

## Описание

**Актуальная версия: v1.0.0**

Интерфейс имеет 4 конечных точки:

- **webhooks** - События сервера,
- **archives** - Архив наименований препаратов,
- **postavka** - Склад аптек,
- **pharmacies** - Аптеки,
- **log** - Журнал вебхуков.

Стандартный ответ сервера

```json
{
  "status": 0,
  "message": "text error",
  "response": []
}
```

Eсли запрос выполнен с ошибкой, то **status** будет отражать код ошибки. В успешных ответах **status** он всегда равен "0". Тело сообщения _response_ может быть null, arrey, object, string, number.

```typescript
interface ResponseApi {
  status: number;
  message: string;
  response: any;
}
```

Запросы выполняются с помощью http протокола на конечную точку мажёрной версии rest api

```url
http://os.auxilium-system.ru/v1/
```

К конечным точкам доступны следующие запросы (опционально) GET, PUT, PATCH, DELETE
В теле запроса передаётся JSON объект. Максимальный размер тела запроса 4Кb.
Сервер поддерживает сжатие данных по протоколу gzip, deflate, br. Предпочтительно использовать для загрузки /v1/archives/ - **deflate**.

## Авторизация

Выполняется стандартная Basic Auth.
В случае ошибки будет возвращён следующий ответ:
```json
{
    "response": null,
    "status": 105,
    "message": "not support request"
}
```
Вебхуки производят вызовы без авторизации.

## Конечные точки

### /v1/webhooks

Запросы к этой точке выполняют полный CRUD доступ для изменения поведения вебхуков. Настраиваются скрипты для вызова, когда на сервере совершаются события.

Интерфейс хранимых данных webhook:

```typescript
interface Webhook {
  id: number;
  /**
   * URL адрес для обработки событий.
   * @example 'https://fake-mm.ru/webhook.php'
   */
  url: string;
  /**
   * Точка активна -
   * на неё будут отправляться события
   */
  active: boolean;
  create_at: Date;
}
```

#### Получить все записи

```url
[GET] /v1/webhooks/
[GET] /v1/webhooks/?page=0
```

Доступны опции:

- page: number - Позволяет отображать вебхуки по 1000 записей на странице. По умолчанию 0. Нумерация страниц производиться от нуля.

```json
{
    "response": [
        {
            "id": 1,
            "url": "https://fake-mm.ru/webhook.php",
            "create_at": "2021-04-23T13:43:52.000Z",
            "active": 1
        },
        {
            "id": 12,
            "url": "https://fake-mm.ru/webhook2.php",
            "create_at": "2021-04-24T23:41:42.000Z",
            "active": 0
        }
    ],
    "status": 0,
    "message": ""
}
```

#### Получить запись по номеру

```url
[GET] /v1/webhooks/:id
```

**Пример ответа:**

```JSON
{
    "response": {
            "id": 1,
            "url": "https://fake-mm.ru/webhook.php",
            "create_at": "2021-04-23T13:43:52.000Z",
            "active": 1
        },
    "status": 0,
    "message": ""
}
```

#### Добавить запись

```url
[PUT] /v1/webhooks/
```

Тело запроса: **application/json**
Поддерживается: **Transfer-Encoding: chunked**

```JSON
{
    "url": "https://yandex.ru",
    "active": false
}
```

**Пример ответа:**

```JSON
{
    "response": [
        {
            "id": 12,
            "url": "https://fake-mm.ru/webhook2.php",
            "create_at": "2021-04-24T23:41:42.000Z",
            "active": 0
        }
    ],
    "status": 0,
    "message": ""
}
```

**Пример неудачного ответа:**

```JSON
{
    "response": null,
    "status": 1062,
    "message": "Duplicate entry 'https://fake-mm.ru/webhook.php' for key 'url'"
}
```

#### Удаление записи

```url
[DELETE] /v1/webhooks/:id
```

Если удаление прошло успешно, то ответ сервера будет true. Если при попытке удалить не было затронуто ни одной строки, то запрос вернёт false;

**Пример ответа:**

```json
{
    "response": true,
    "status": 0,
    "message": ""
}
```

#### Редактирование данных


```url
[PATCH] /v1/webhooks/:id
```  
Тело запроса: **application/json**
Поддерживается: **Transfer-Encoding: chunked**

```JSON
{
    "url": "https://yandex.ru/webhook.php",
    "active": false
}
```

Поддерживается редактирование url, active;
В случае успеха, будет возвращён ответ в виде true, если запись не была найдена, вернётся ответ false;

Пример ответа:

```json
{
    "response": true,
    "status": 0,
    "message": ""
}
```

### /v1/archives

#### Получить весь архив
```url
[GET] /v1/archives/
```

Также можно использовать пагинацию по 1000 шт. 
```url
[GET] /v1/archives/?page=0
```
**Пример ответа сервера:**
```json
{
    "response": [
        {
            "scu": 123770,
            "name": "ОЧКИ РАЛЬФ  1,50 /АРТ.5858/"
        }, 
        // ...
        {
            "scu": 10912690,
            "name": "ТОНУС ЭЛАСТ.КОЛГОТКИ"
        }

    ],
    "status": 0,
    "message": ""
}
```
Поддерживается фильтр по названию

```url
[GET] /v1/archive/?name=...
```
Пример:

```url
[GET] /v1/archive/?name=Synchrony
[GET] /v1/archive/?name=%Synchrony
[GET] /v1/archive/?name=Synchrony%
[GET] /v1/archive/?name=%Synchrony%
```
Соответственно - "точное совпадение", "окончивается на", "начинается на", "Имеет вхождение"
Поиск регистронезависемый.

#### Получить товар по номеру
Можно получить любой товар, не обязательно из группы оптика
```url
[GET] /v1/archives/:id
```

**Пример ответа сервера:**
```json
{
    "response": {
        "scu": 1012,
        "name": "ПРЕДНИЗОЛОН ГЕМИСУКЦИНАТ 25МГ. №10 АМП."
    },
    "status": 0,
    "message": ""
}
```

**Неудачный ответ сервера**
```json
{
    "response": null,
    "status": 101,
    "message": "not support request"
}
```
### /v1/postavka

#### Получить весь склад 

```url
[GET] /v1/postavka
```

Поддерживается пагинация ?page=0 с нулевой страницы.

**Пример ответа:**
```json
{
    "response": [
        {
            "pid": 200100249810,
            "aid": 1,
            "qty": "1.00",
            "price": "600.00",
            "scu": 4389395,
            "GDATE": "2099-12-30T21:00:00.000Z"
        },
        // ... 
        {
            "pid": 200500260598,
            "aid": 5,
            "qty": "2.00",
            "price": "3720.00",
            "scu": 4389686,
            "GDATE": "2099-12-30T21:00:00.000Z"
        }
    ],
    "status": 0,
    "message": ""
}
```

#### Получить товар по номеру или штрихкоду

```
[GET] /v1/postavka/:scu
[GET] /v1/postavka/:pid
```
В отличии от аналогичных методов других конечных точек, этот возвращает массив результатов.

**Пример:**
```json
{
    "response": [
        {
            "pid": 200200095755,
            "aid": 2,
            "qty": "1.00",
            "price": "2400.00",
            "scu": 4389686,
            "GDATE": "2099-12-30T21:00:00.000Z"
        }
    ],
    "status": 0,
    "message": ""
}
```

### /v1/pharamcies

#### Получить все аптеки

```url
[GET] /v1/pharmacies/
```

```json
{
    "response": [
        {
            "id": 1,
            "name": "Ярославское шоссе. 107",
            "phone": "74954191303",
            "queue": 4
        },
        // ...
        {
            "id": 5,
            "name": "ул. Лестева д.11",
            "phone": "74997024303",
            "queue": 2
        }
    ],
    "status": 0,
    "message": ""
}
```


#### Получить аптеку по номеру


```url
[GET] /v1/pharmacies/:id
```

**Пример ответа:**
```json
{
    "response": {
        "id": 2,
        "name": "1-ый Неопалимовский пер.д.8 ",
        "phone": "74954191203",
        "queue": 3
    },
    "status": 0,
    "message": ""
}
```

## /v1/log

#### Получить все логи 

```url
[GET] /v1/log/
[GET] /v1/log/?error
[GET] /v1/log/?chore
[GET] /v1/log/?page=0
```

**Пример ответа:**
```json
{
    "response": [
        "[SUCCESS_URL] 2021-04-25T10:04:01.786Z https://fake-mm.ru/webhook.php | {\"eventName\":\"start\",\"data\":[],\"date\":1619345041746}",
        "[SUCCESS_URL] 2021-04-25T10:06:21.758Z https://fake-mm.ru/webhook.php | {\"eventName\":\"stop\",\"data\":[],\"date\":1619345181743}",
    ],
    "status": 0,
    "message": ""
}
```

Получение файла логов ошибок, работы, рутинных операций. Доступна пагинация по 100 позиций журнала.


## Работа с веб хуком

Вебхуки отправляют на зарегистрированный и активный адрес POST запрос c полем **"event"** в теле запроса. 
В этом поле хранится строка JSON с данными о событии вида:

```typescript
interface Postavka {
    pid: number;
    aid: number;
    qty: number;
    price: number;
    scu: number;
    GDATE: string;
}

interface RequestJSONWebhook {
    /**
     * Название события
     */
    eventName: string;
    /**
     * Полезная нагрузка события
     */
    data: Postavka[];
    /**
     * Дата события
     */
    date: number;

    /**
     * Уникальный идентификатор
     */
    __key__: string;
}

interface RequestPOSTWebhook {
    /**
     * Преобразуется в RequestJSONWebhook
     */
    event: string;
}
```

Цены указаны вкопейках.
Поддерживаются следующие события

| Название | Описание |
|:---:|:---|
| start | Начала работы сервиса |
| stop | Остановка работы сервиса |
| add | Приход на склад |
| delete | Уход со склада |
| update | Изменение параметров позиции |

Уникальный идентификатор товара на складе является **pid + aid** 

**Пример запроса сервера:**

```json
{
    "eventName" : "update",
    "data": [
        {
            "pid": 200300420081,
            "aid": 3,
            "qty": 1,
            "price": 195000,
            "scu": 193683,
            "GDATE": "2024-01-08",
            "__key__": "000200300420081-000000000000003"
        }
    ],
    "date": 1619343812290
}
```
Обновление происходит, когда были изменены следующие параметры:
* Цена
* Количество
* Аптека

Если количество **0** то будет вызванно событие **"delete"**.
Когда кассир вносит товар в чек, он удаляется/списывается, если чек отменён то товар возвращается через событие **"update"** или **"add"**

Ответ серверу должен приходить в виде строки с JSON структурой

```php
echo '{"status": 0}';
```

