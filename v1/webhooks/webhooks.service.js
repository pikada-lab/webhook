 class WebhookService {

     constructor(connect) {
         this.pageLength = 1000;
         this.connect = connect;
     }

     getAll(query) {
         // console.log(query.page);
         if (!query.has("page")) query.append("page", 0);
         const limit = `LIMIT ${+query.get("page") * this.pageLength}, ${ +this.pageLength}`;

         return this.connect.query(`SELECT * FROM webhook ${limit}`);
     }

     getByID(id) {
         return this.connect.query("SELECT * FROM `webhook` WHERE `id` = ? LIMIT 0, 1", [id]);
     }
     delete(id) {
         return this.connect.query("DELETE FROM `webhook` WHERE `id` = ? LIMIT 1", [id]);
     }
     update(id, data) {
         let req = [];
         let reqSQL = [];
         if (typeof data.url != 'undefined') {
             req.push(data.url);
             reqSQL.push("`url` = ?")
         }
         if (typeof data.active != 'undefined') {
             req.push(data.active);
             reqSQL.push("`active` = ?")
         }
         req.push(id)
         return this.connect.query("UPDATE `webhook` SET  " + reqSQL.join(", ") + " WHERE `webhook`.`id` = ?", req);
     }
     add(data) {
         console.log("add", data);
         return this.connect.query("INSERT INTO `webhook` (`id`, `url`, `create_at`, `active`) VALUES (NULL, ?, CURRENT_TIMESTAMP, ?);", [data.url, data.active])
     }
 }

 module.exports.WebhookService = WebhookService;