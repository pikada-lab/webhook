 class WebhookController {
     constructor(service) {
         this.webhookService = service;
     }
     async handle(path, method, params, data) {
         let id = null;
         if (path && !!+path) {
             id = +path
         }
         console.log(`[${method}]`, path, "|", params, "|", data);

         switch (method) {
             case "get":
                 if (!id) {
                     return this.webhookService.getAll(params)
                 } else {
                     return this.webhookService.getByID(id).then(res => {
                         return Promise.resolve(res[0]);
                     });
                 }
             case "put":
                 return this.webhookService.add(data).then(res => {
                     return this.webhookService.getByID(res.insertId);
                 });
             case "delete":
                 return this.webhookService.delete(id).then(res => {
                     return Promise.resolve(res.affectedRows > 0);
                 });
             case "patch":
                 return this.webhookService.update(id, data).then(res => {
                     return Promise.resolve(res.affectedRows > 0);
                 });
             default:
                 return Promise.reject("Bad request, support method GET, PUT, DELETE, PATCH");
         }
     }
 }

 module.exports.WebhookController = WebhookController;