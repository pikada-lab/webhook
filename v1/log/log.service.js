const { readFile } = require("fs");
const { promisify } = require("util");

class LogService {
    constructor() {
        this.pageLength = 1000;
    }

    getAll(query) {
        // console.log(query.page); 
        if (query.has("error")) {
            return promisify(readFile)("./error.log", 'utf8');
        } else if (query.has("chore")) {
            return promisify(readFile)("./chore.log", 'utf8');
        } else {
            return promisify(readFile)("./success.log", 'utf8');
        }
    }

    getByID(id) {
        return Promise.reject({ status: 11, message: "Delete not support" });
    }

    delete(id) {
        return Promise.reject({ status: 11, message: "Delete not support" });
    }

    update(id, data) {
        return Promise.reject({ status: 12, message: "Update not support" });
    }

    add(data) {
        return Promise.reject({ status: 13, message: "Add not support" });
    }
}


module.exports.LogService = LogService;