 class PostavkaController {
     constructor(service) {
         this.service = service;
     }
     async handle(path, method, params, data) {
         let id = null;
         if (path && !!+path) {
             id = +path
         }
         console.log(`[${method}]`, path, "|", params, "|", data);

         switch (method) {
             case "get":
                 if (!id) {
                     return this.service.getAll(params).then(res => {
                         return Promise.resolve(res.map(r => {
                             r.qty = Math.round(+r.qty);
                             r.price = Math.round(r.price * 100);
                             let GDATE = new Date(r.GDATE);
                             if (GDATE) {
                                 GDATE.setMinutes(GDATE.getMinutes() - GDATE.getTimezoneOffset());
                                 r.GDATE = GDATE.toJSON();
                                 if (r.GDATE == "1899-11-29T23:59:43.000Z") {
                                     r.GDATE = null;
                                 }
                                 if (r.GDATE != null) {
                                     r.GDATE = r.GDATE.substr(0, 10);
                                 }
                             }
                             return r;
                         }))
                     })
                 } else {
                     return this.service.getByID(id).then(res => {
                         return Promise.resolve(res.map(r => {
                             r.qty = Math.round(+r.qty);
                             r.price = Math.round(r.price * 100);
                             let GDATE = new Date(r.GDATE);
                             if (GDATE) {
                                 GDATE.setMinutes(GDATE.getMinutes() - GDATE.getTimezoneOffset());
                                 r.GDATE = GDATE.toJSON();

                                 if (r.GDATE == "1899-11-29T23:59:43.000Z") {
                                     r.GDATE = null;
                                 }
                                 if (r.GDATE != null) {
                                     r.GDATE = r.GDATE.substr(0, 10);
                                 }
                             }
                             return r;
                         }))
                     })
                 }
             case "put":
                 console.log("put", data);
                 return this.service.add(data).then(res => {
                     console.log(res);
                     return this.service.getByID(res.insertId);
                 });
             case "delete":
                 return this.service.delete(id).then(res => {
                     return Promise.resolve(res.affectedRows > 0);
                 });
             case "patch":
                 return this.service.update(id, data).then(res => {
                     return Promise.resolve(res.affectedRows > 0);
                 });
             default:
                 return Promise.reject("Bad request, support method GET, PUT, DELETE, PATCH");
         }

     }
 }

 module.exports.PostavkaController = PostavkaController;