 class PostavkaService {
     constructor(connect) {
         this.pageLength = 1000;
         this.connect = connect;
     }

     getAll(query) {
         // console.log(query.page);
         let limit = "";
         if (query.has("page")) {
             limit = `LIMIT ${+query.get("page") * this.pageLength}, ${ +this.pageLength}`;
         }
         return this.connect.query(`SELECT P.pid, P.aid, P.qty, P.price, P.scu, GDATE FROM postavka P LEFT JOIN scuArchive S ON S.scu = P.scu WHERE P.aid < 6 AND S.\`group\` IN (?) AND P.qty > 0 ${limit}`, [
             [32, 15]
         ]);
     }

     getByID(id) {
         return this.connect.query("SELECT P.pid, P.aid, P.qty, P.price, P.scu, GDATE  FROM `postavka` P WHERE P.`aid` < 6 AND P.qty > 0 AND (P.`scu` = ? OR P.`pid` = ?) ", [id, id]);
     }

     delete(id) {
         return Promise.reject({ status: 11, message: "Delete not support" });
     }

     update(id, data) {
         return Promise.reject({ status: 12, message: "Update not support" });
     }

     add(data) {
         return Promise.reject({ status: 13, message: "Add not support" });
     }
 }

 module.exports.PostavkaService = PostavkaService;