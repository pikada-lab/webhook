 class PharmacyService {
     constructor(connect) {
         this.pageLength = 1000;
         this.connect = connect;
     }

     getAll(query) {
         // console.log(query.page);
         let limit = "";
         if (query.has("page")) {
             limit = `LIMIT ${+query.get("page") * this.pageLength}, ${ +this.pageLength}`;
         }
         return this.connect.query(`SELECT id, name,  phone, queue FROM ter_apt WHERE client_id = 1 AND open = 1 ${limit}`);
     }

     getByID(id) {
         return this.connect.query("SELECT `id`, `name`,  `phone`, `queue` FROM `ter_apt` WHERE `client_id` = 1 AND `id` = ? LIMIT 0, 1", [id]);
     }

     delete(id) {
         return Promise.reject({ status: 11, message: "Delete not support" });
     }

     update(id, data) {
         return Promise.reject({ status: 12, message: "Update not support" });
     }

     add(data) {
         return Promise.reject({ status: 13, message: "Add not support" });
     }
 }

 module.exports.PharmacyService = PharmacyService;