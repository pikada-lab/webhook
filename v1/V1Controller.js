const { isContext } = require("./isContext");

const { ArchiveController } = require("./archives/archives.controller");
const { ArchiveService } = require("./archives/archives.service");

const { LogController } = require("./log/log.controller");
const { LogService } = require("./log/log.service");

const { PharmacyController } = require("./pharmacies/pharmacies.controller");
const { PharmacyService } = require("./pharmacies/pharmacies.service");

const { PostavkaController } = require("./postavka/postavka.controller");
const { PostavkaService } = require("./postavka/postavka.service");

const { WebhookController } = require("./webhooks/webhooks.controller");
const { WebhookService } = require("./webhooks/webhooks.service");

class V1Controller {
    constructor(connect) {
        this.archiveController = new ArchiveController(new ArchiveService(connect));
        this.logController = new LogController(new LogService(connect));
        this.pharmacyController = new PharmacyController(new PharmacyService(connect));
        this.postavkaController = new PostavkaController(new PostavkaService(connect));
        this.webhookController = new WebhookController(new WebhookService(connect));
    }
    async handle(path, method, params, data) {
        console.log(`[${method}]`, path, "|", params, "|", data);
        if (!path) return Promise.reject("Bad Request");
        if (isContext("webhooks", path)) {
            return this.webhookController.handle(path.substr("webhooks/".length), method, params, data);
        } else if (isContext("archives", path)) {
            return this.archiveController.handle(path.substr("archives/".length), method, params, data);
        } else if (isContext("postavka", path)) {
            return this.postavkaController.handle(path.substr("postavka/".length), method, params, data);
        } else if (isContext("pharmacies", path)) {
            return this.pharmacyController.handle(path.substr("pharmacies/".length), method, params, data);
        } else if (isContext("log", path)) {
            return this.logController.handle(path.substr("log/".length), method, params, data);
        }
        return Promise.reject("Bad Request");
    }
}


module.exports.V1Controller = V1Controller