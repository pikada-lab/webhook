 class ArchiveController {
     constructor(service) {
         this.service = service;
     }
     async handle(path, method, params, data) {
         let id = null;
         if (path && !!+path) {
             id = +path
         }
         console.log(`[${method}]`, path, "|", params, "|", data);

         switch (method) {
             case "get":
                 if (!id) {
                     return this.service.getAll(params)
                 } else {
                     return this.service.getByID(id).then(res => {
                         return Promise.resolve(res[0]);
                     });
                 }
             case "put":
                 console.log("put", data);
                 return this.service.add(data).then(res => {
                     console.log(res);
                     return this.service.getByID(res.insertId);
                 });
             case "delete":
                 return this.service.delete(id).then(res => {
                     return Promise.resolve(res.affectedRows > 0);
                 });
             case "patch":
                 return this.service.update(id, data).then(res => {
                     return Promise.resolve(res.affectedRows > 0);
                 });
             default:
                 return Promise.reject("Bad request, support method GET, PUT, DELETE, PATCH");
         }

     }
 }
 module.exports.ArchiveController = ArchiveController