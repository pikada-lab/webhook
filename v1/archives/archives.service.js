 class ArchiveService {
     constructor(connect) {
         this.pageLength = 1000;
         this.connect = connect;
     }

     getAll(query) {
         // console.log(query.page);
         let limit = "";
         if (query.has("page")) {
             limit = `LIMIT ${+query.get("page") * this.pageLength}, ${ +this.pageLength}`;
         }
         let req = [
             [32, 15]
         ];
         let queryStr = "";
         if (query.has("name")) {
             req.push(query.get("name"))
             queryStr = `AND NAME LIKE  ?`;
         }
         return this.connect.query(`SELECT scu, name FROM scuArchive WHERE \`group\` IN (?) ${queryStr} ${limit}`, req);
     }

     getByID(id) {
         return this.connect.query("SELECT  `scu`, `name`  FROM `scuArchive` WHERE `scu` = ? LIMIT 0, 1", [id]);
     }

     delete(id) {
         return Promise.reject({ status: 11, message: "Delete not support" });
     }

     update(id, data) {
         return Promise.reject({ status: 12, message: "Update not support" });
     }

     add(data) {
         return Promise.reject({ status: 13, message: "Add not support" });
     }
 }

 module.exports.ArchiveService = ArchiveService;