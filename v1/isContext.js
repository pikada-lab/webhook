module.exports.isContext = (name, path) => {
    return path === name || path.startsWith(`${name}/`)
}