
# Создание защищённого соединения

```bash
openssl genrsa -out ryans-key.pem 2048
openssl req -new -sha256 -key ryans-key.pem -out ryans-csr.pem
openssl x509 -req -in ryans-csr.pem -signkey ryans-key.pem -out ryans-cert.pem
```

Этот вид общения предназначен для общения в обе стороны. Сокет поддерживает до 10 подключений. 

## Сервер 

```javascript 
const s = new WSocket({
    key: "./private-key.pem",
    cert: "./public-cert.pem",
    port: 8124,
    host: 'localhost'
});

s.init();

```
### Соыбтия

WSocket.events


| Название             |   параметр    | описание                                 |
| :------------------- | :-----------: | :--------------------------------------- |
| construct            |       -       | Был вызван конструктор класса WSocket    |
| connect              |       -       | Началось подключение клиента             |
| client/connected     |  id: number   | Подключён клиента с номером id           |
| client/disconnected  |  id: number   | Отключение клиента c номером id          |
| client/message/:id   |  event: any   | пришло сообщение от клиента id           |
| client/message/:name |  event: any   | пришло сообщение от клиентов name client |
| client/message/*/    |  event: any   | пришло сообщение от любого клиента       |
| server/error         |   err: any    | ошибка сервера                           |
| server/listen        | port: number  | сервер был установлен на порт port       |
| server/events        | event: string | было вызванно событие с названием event  |

### Методы


| Название                                                     |            ответ            | описание                                 |
| :----------------------------------------------------------- | :-------------------------: | :--------------------------------------- |
| getAll()                                                     | {id: number, name:string}[] | получить массив с объектов {id, name}    |
| getNames()                                                   |          string[]           | Получить массив name клиентов            |
| hasNames(name)                                               |           boolean           | Проверяет наличие клиента по имени       |
| getIDs()                                                     |          string[]           | Получить массив id клиентов              |
| sendToAll(eventName: string, event: string)                  |            void             | Отправить сообщение всем клиентам        |
| sendToName(eventName: string, name: string, message: string) |            void             | Отправить сообщение всем с именем name   |
| sendToId(eventName: string, id: string, message: string)     |            void             | Отправить сообщение клиенту с номером id |
| getStream(id)                                                |        DuplexStream         | Получить поток сокета                    |

## Клиент

options

* name: string - произвольное название сервера

```javascript 
const client = new ClientSocket({
    port: 8124,
    host: 'localhost',
    name: "auxilium-server",
    key: "./private-key.pem",
    cert: "./public-cert.pem"
});
client.init();
```


### Соыбтия



| Название              |   параметр    | описание                                   |
| :-------------------- | :-----------: | :----------------------------------------- |
| construct             |  id: string   | Был вызван конструктор класса ClientSocket |
| connect               |  id: string   | Началось подключение клиента               |
| disconnect            |  id: number   | Подключён клиента с номером id             |
| client/disconnected   |  id: number   | Отключение клиента c номером id            |
| server/message/:event | data: string  | пришло событие event                       |
| server/events         | event: string | Название события                           |
| server/error          |   err: any    | Сообщение об ошибке соединения             |

### Методы

 send(event, message)  - отправить на сервере событие event с данными message
 
## Протокол общения

1. Передать свой уникальный номер и название сервиса
```
12\r\nserver name\r\n
```
1. Начать приём и передачу коротких JOSN сообщений.

Примечание, если нужно передать большой JSON массив, то следует продумать это так, что бы передавалось событие начала передачи, событие chank и событие конца передачи.