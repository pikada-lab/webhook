const { promisify } = require("util");
const { readFile } = require('fs');
const { Server } = require("./server/Server");
const { Connect } = require("./database/connect");
const { VersionController } = require("./VersionController");
const { join } = require("path");

class App {
    constructor(env) {
        this.config = { spawn: { watcher: false } }
        this.env = env;
    }

    async start() {
        try {
            await this.initConfigEnviroment(this.env);
            await this.initConfig();
            await this.initDatabase();
            if (this.config.spawn.watcher) this.spawnWatcher();
            this.controller = new VersionController(this.connect);
            this.server = new Server(this.config, this.controller);
            this.server.start();
        } catch (ex) {
            console.log(ex.message);
        }
    }

    async initConfigEnviroment(env) {
        this.configFile = (env.MODE_ENV) ? env.MODE_ENV : 'default';
        this.config = Object.assign(this.config, {
            root: __dirname,
            configDir: join(__dirname, 'config'),
            configFile: join(__dirname, 'config', `${this.configFile}.json`)
        });
    }
    async initConfig() {
        this.configString = await promisify(readFile)(this.config.configFile);
        this.config = Object.assign(this.config, JSON.parse(this.configString));
    }

    async initDatabase() {
        this.connect = new Connect(this.config.database);
    }

    spawnWatcher() {
        const { spawn } = require('child_process');
        const ls = spawn('node', ['./watcher/watcher.js']);

        ls.stdout.on('data', (data) => {
            console.log(`stdout: ${data}`);
        });
        ls.stderr.on('data', (data) => {
            console.error(`stderr: ${data}`);
        });
        ls.on('close', (code) => {
            console.log(`child process exited with code ${code}`);
        });
    }

}

module.exports.App = App;