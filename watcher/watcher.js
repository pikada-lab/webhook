const mysql = require('mysql2');
const { Comparator } = require("aux-comparator");;
const { URL } = require('url');
const { FormData } = require("./FormData");
const { HttpClient } = require("./HttpClient");
let httpClient = new HttpClient();
const { readFile, writeFile, appendFile } = require("fs");
const { promisify } = require("util");
const process = require('process');
const { Connect } = require('../database/connect');

const { WSocket } = require("../ws/WSocket.js"); 
 

var fieldEntitie = ['pid', 'aid', 'qty', 'price', 'scu', 'GDATE'];
(async function() {
    configFile = (process.env.MODE_ENV) ? process.env.MODE_ENV : 'default';
    configString = await promisify(readFile)(`../config/${ configFile}.json`);
    config = JSON.parse(configString);
    let ws = new WSocket(config.socket);
    ws.init();
    ws.sendToAll("start", Date.now().toString());
    const connect = new Connect(config.database);
    let urls = await getWebhook(connect);
    for (let url of urls) {
        if (url.status === 0) {
            await hook(url.url, { eventName: "start", data: [], date: Date.now() });
        }
    }

    process.on('uncaughtException', async(error) => {
        await promisify(appendFile)("./error.log", "\n" + error);
        process.exit(1);
    });

    process.on('SIGINT', async(error) => {

    ws.sendToAll("stop", Date.now().toString());
        let urls = await getWebhook(connect);
        for (let url of urls) {
            if (url.status === 0) {
                await hook(url.url, { eventName: "stop", data: [], date: Date.now() });
            }
        }
        process.exit(1);
    });
    var oldTable = [];
    try {
        let oldTableString = await promisify(readFile)("./.laststate.json", { encoding: 'utf8' });
        if (!oldTableString) {
            oldTable = await getAll(connect);
        } else {
            oldTable = JSON.parse(oldTableString)
        }
    } catch (ex) {
        oldTable = await getAll(connect);
    }
    setInterval(async _ => {
        var newTable = await getAll(connect);
        let tmpTable = Array.from(newTable);
        let time = Date.now();
        new Comparator()
            .setNewArray(newTable)
            .setOldArray(oldTable)
            .setIndexes(["pid", "aid"])
            .setCompareFunction((l, r) => {
                return l.price === r.price && l.qty === r.qty && l.aid == r.aid
            })
            .sortNewArray()
            .sortOldArray()
            .softCompare()
            .afterComare(async(r) => {
                time = Date.now() - time;
                if (r.update.length || r.delete.length || r.add.length) {
                    let urls = await getWebhook(connect);
                    if (r.update.length) {
                        console.log("update")
                        await promisify(appendFile)("./chore.log", "\n [CHORE] " + new Date().toJSON() + " | update " + r.update.length + " item(s)");
                        console.table(r.update.map(r => r.old).slice(0, 10));
                        console.table(r.update.map(r => r.new).slice(0, 10));
                        ws.sendToAll("update", JSON.stringify({ eventName: "update", data: r.update, date: Date.now() }))
                        urls.forEach(url => {
                            if (url.status === 0)
                                hook(url.url, { eventName: "update", data: r.update.map(r => r.new), date: Date.now() });
                        })
                    }
                    if (r.delete.length) {
                        console.log("delete")
                        await promisify(appendFile)("./chore.log", "\n [CHORE] " + new Date().toJSON() + " | delete " + r.delete.length + " item(s)");
                        console.table(r.delete.slice(0, 10));
                        ws.sendToAll("delete", JSON.stringify({ eventName: "delete", data: r.delete, date: Date.now() }))
                        urls.forEach(url => {
                            if (url.status === 0)
                                hook(url.url, { eventName: "delete", data: r.delete, date: Date.now() });
                        })
                    }
                    if (r.add.length) {
                        console.log("add " + r.add.length)
                        await promisify(appendFile)("./chore.log", "\n [CHORE] " + new Date().toJSON() + " | add " + r.add.length + " item(s)");
                        console.table(r.add.slice(0, 10));
                        ws.sendToAll("add", JSON.stringify({ eventName: "add", data: r.add, date: Date.now() }))
                        urls.forEach(url => {
                            if (url.status === 0)
                                hook(url.url, { eventName: "add", data: r.add, date: Date.now() });
                        })
                    }
                    oldTable = tmpTable.map(r => {
                        let obj = {};
                        for (let i of fieldEntitie) {
                            obj[i] = r[i];
                        }
                        return obj;
                    });
                    await promisify(writeFile)("./.laststate.json", JSON.stringify(oldTable), { encoding: 'utf8' });
                    await promisify(appendFile)("./chore.log", "\n [CHORE] " + new Date().toJSON() + " | Compare diffrent content " + (time / 1000) + "s | " + tmpTable.length + " item(s)");
                } else {
                    await promisify(appendFile)("./chore.log", "\n [CHORE] " + new Date().toJSON() + " | Compare equal content " + (time / 1000) + "s | " + tmpTable.length + " item(s)");
                    oldTable = tmpTable.map(r => {
                        let obj = {};
                        for (let i of fieldEntitie) {
                            obj[i] = r[i];
                        }
                        return obj;
                    });
                }

                // console.log(newTable,oldTable );
            })
    }, 5000)
})()

async function hook(url, event) {
    try {
        console.log(event.eventName)
        const postData = JSON.stringify(event);
        const f = new FormData();
        f.append("event", postData);
        let result = await httpClient.post(url, f, { "user-agent": "auxilium-system: v1 // robot" });
        console.log(url.toString(), result);
        await promisify(appendFile)("./success.log", "\n[SUCCESS_URL] " + new Date().toJSON() + " " + url.toString() + ' | ' + JSON.stringify(event));
    } catch (ex) {
        console.log("err url", url.toString(), event)
        await promisify(appendFile)("./error.log", "\n[ERROR_URL] " + new Date().toJSON() + " " + url.toString() + ' - ' + ex.message + ' | ' + JSON.stringify(event));
        console.log(ex);
    }
}

function getWebhook(connect) {
    return new Promise((res, rej) => {

        // console.time("Start sql request webhook")
        connect.connection.query(
            'SELECT `url`  FROM `cito3_orto`.`webhook` WHERE `active`= 1',
            function(err, results) {
                // console.timeEnd("Start sql request webhook")
                results = results.map(r => {
                        r.source = r.url
                        r.status = 0;
                        try {
                            r.url = new URL(r.url);
                        } catch (ex) {
                            r.status = 1;
                            r.message = ex.message;
                        }
                        return r;
                    })
                    // console.log("count items: ", results.length);
                    // connection.destroy();
                res(results);
            }
        );
    })
}

function getAll(connect) {
    return new Promise((res, rej) => {

        // console.time("Start sql request")
        connect.connection.query(
            'SELECT P.`pid`,P.`aid`,P.`qty`,P.`price`, P.`scu`, P.`GDATE`  FROM `cito3_orto`.`postavka` P LEFT JOIN `cito3_orto`.`scuArchive` S ON S.`scu` = P.`scu`  WHERE   S.\`group\` IN (?) AND P.`aid` IN (?) AND P.`qty` != ? ORDER BY P.`pid`, P.`aid` ASC  ', [
                [32, 15, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 33, 34],
                [1, 2, 3, 5], 0
            ],
            function(err, results) {
                // console.timeEnd("Start sql request")
                results = results.map(r => {
                        r.qty = Math.round(r.qty)
                        r.price = Math.round(r.price * 100)
                        let GDATE = new Date(r.GDATE);
                        if (GDATE) {
                            GDATE.setMinutes(GDATE.getMinutes() - GDATE.getTimezoneOffset());
                            r.GDATE = GDATE.toJSON();
                            if (r.GDATE == "1899-11-29T23:59:43.000Z") {
                                r.GDATE = null;
                            }
                            if (r.GDATE != null) {
                                r.GDATE = r.GDATE.substr(0, 10);
                            }
                        }
                        return r;
                    })
                    // console.log("count items: ", results.length);
                    // connection.destroy();
                res(results);
            }
        );
    })
}